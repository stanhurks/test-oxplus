/**
 * All HTTP related functionality required for the application.
 *
 * @author Hurks IT
 */
const $http = {
    options: {
        apiBase: ''
    }
}
export default {
    /**
     * Makes a get request
     */
    get: function (url, headers) {
        return this.request('GET', $http.options.apiBase + url, {}, headers || {})
    },

    /**
     * Makes a post request
     */
    post: function (url, body, headers) {
        return this.request('POST', $http.options.apiBase + url, body || {}, headers || {})
    },

    /**
     * Makes a put request
     */
    put: function (url, body, headers) {
        return this.request('PUT', $http.options.apiBase + url, body || {}, headers || {})
    },

    /**
     * Makes a delete request
     */
    delete: function (url, body, headers) {
        return this.request('DELETE', $http.options.apiBase + url, body || {}, headers || {})
    },

    /**
     * Makes a XMLHttpRequest and return the response as a promise.
     */
    request: function (method, url, body, headers) {
        return new Promise((resolve, reject) => {
            var request = new XMLHttpRequest()
            request.open(method, url)
            request.setRequestHeader('Content-Type', 'application/json')
            if (headers != null) {
                Object.keys(headers).forEach(key => {
                    request.setRequestHeader(key, headers[key])
                })
            }
            request.onload = () => {
                if (request.status >= 200 && request.status < 300) {
                    resolve({
                        data: request.response,
                        status: request.status
                    })
                } else {
                    reject({
                        data: request.response,
                        status: request.status
                    })
                }
            }
            request.onerror = () => {
                reject(request.statusText)
            }
            request.send(JSON.stringify(body))
        })
    }
}